#include <array>
#include <iostream>

#include "ConcurrentHash.hpp"

int add[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 };
int find[] = { 4, 5, 26, 7, 8, 9, 20, 41, 12, 13, 54, 25, 17, 18, 19 };
int del[] = { 3, 4, 7, 10, 21, 12, 13, 24, 15, 16 };

int main()
{
	ConcurrentHash<int, 107> hashTable;
	std::cout << "Добавление:";
	for (std::size_t i = 0; i < sizeof(add) / sizeof(int); ++i) {
		hashTable.add(add[i]);
		if (i != 0)
			std::cout << ", ";
		std::cout << add[i];
	}
	std::cout << std::endl;

	std::cout << "Поиск" << std::endl;
	for (std::size_t i = 0; i < sizeof(find) / sizeof(int); ++i) {
		std::cout << "Элемент " << find[i] << " ";
		if (hashTable.find(find[i]))
			std::cout << "присутвует";
		else
			std::cout << "отсутвует";
		std::cout << std::endl;
	}

	std::cout << "Удаление" << std::endl;
	for (std::size_t i = 0; i < sizeof(del) / sizeof(int); ++i) {
		hashTable.remove(del[i]);
		if (i != 0)
			std::cout << ", ";
		std::cout << del[i];
	}
	std::cout << std::endl;

	std::cout << "Поиск" << std::endl;
	for (std::size_t i = 0; i < sizeof(find) / sizeof(int); ++i) {
		std::cout << "Элемент " << find[i] << " ";
		if (hashTable.find(find[i]))
			std::cout << "присутвует";
		else
			std::cout << "отсутвует";
		std::cout << std::endl;
	}

	return 0;
}
