#include <array>
#include <random>
#include <chrono>
#include <thread>
#include <iostream>

#include <pthread.h>

#include "ConcurrentHash.hpp"

enum { SIZE_HASH_TABLE = 10000007, N = 5000000, I = 2000000, S = 3000000, D = 1000000 };

typedef ConcurrentHash<int, SIZE_HASH_TABLE> hashTable;

hashTable table;

void threadWork(uint32_t block, uint32_t numbersBlock) {
	std::mt19937 rand(std::chrono::system_clock::now().time_since_epoch().count());
	for (uint32_t i = block; i < I; i += numbersBlock)
		table.add(rand());

	for (uint32_t i = block; i < S; i += numbersBlock)
		table.find(rand());

	for (uint32_t i = block; i < D; i += numbersBlock)
		table.remove(rand());
}

int main()
{
	std::mt19937 rand(std::chrono::system_clock::now().time_since_epoch().count());

	for (int i = 0; i < N; ++i)
		table.add(rand());

	int numberThreads = std::thread::hardware_concurrency();
	std::vector<std::thread> threads;
	for (int proc = 0; proc < numberThreads; ++proc) {
		cpu_set_t cpu;
		CPU_ZERO(&cpu);
		CPU_SET(proc + 1, &cpu);
		threads.push_back(std::thread(threadWork, proc, numberThreads));
		pthread_setaffinity_np(threads[proc].native_handle(), sizeof(cpu_set_t), &cpu);
	}
	for (int proc = 0; proc < numberThreads; ++proc)
		threads[proc].join();
	return 0;
}
