#include <array>
#include <mutex>
#include <memory>
#include <iostream>
#include <functional>

template<typename Value, std::size_t sizeTable> class ConcurrentHash {
private:
// Элемент односвязного списка который формируется при коллизиях
	struct ElementCollision {
		std::mutex mutex;
		std::shared_ptr<Value> value;
		std::unique_ptr<ElementCollision> next;

		ElementCollision(): next() {};
		ElementCollision(const Value &val): value(std::make_shared<Value>(val)) {};
	};

	std::array<std::unique_ptr<ElementCollision>, sizeTable> m_Table; // Таблица указателей на начало списка

	std::size_t hashIndex(const Value &val) const;	// отображает ключа на индекс хэш-таблицы
	bool foreachList(const Value &val, bool add);	// обход по списку коллизий, если флаг добавления выставлен, то ключ добавляется в конец списка

public:
// удаляем копирующий конструктор и оператор присваивание, ибо копирование объекта, который в этот момент может быть изменяем в другом потоке, дурная мысль
	ConcurrentHash(const ConcurrentHash &rval) = delete;
	ConcurrentHash operator=(const ConcurrentHash &rval) = delete;

	ConcurrentHash();

	void add(const Value &val);			// Добавление ключа в хеш
	void remove(const Value &val);		// Удаление ключа
	bool find(const Value &val);		// Поиск ключа, true если есть
};

template<typename Value, std::size_t sizeTable> ConcurrentHash<Value, sizeTable>::ConcurrentHash() {
// В конструктере по умолчанию, для всех элементам таблицы присваиваем указатель на фиктивный элемент односвязных списков
// поле value указывает на nullptr, без этого элемента можно обойтись, но так просто проще, это элемент будет вегда
	for (std::size_t i = 0; i < sizeTable; ++i)
		m_Table[i] = std::unique_ptr<ElementCollision>(new ElementCollision());
}

template<typename Value, std::size_t sizeTable> std::size_t ConcurrentHash<Value, sizeTable>::hashIndex(const Value &val) const {
// для хеша использую хеш функцию из std std::hash, прямого отношения к многопоточности не имеет, доебаться не должен, но все же
	std::hash<Value> hash;
	return hash(val) % sizeTable;	// хеш-значение делим по остатку на размер таблицы, чтоб значение попало в эту самую таблицу
}

template<typename Value, std::size_t sizeTable> bool ConcurrentHash<Value, sizeTable>::foreachList(const Value &val, bool add) {
// Функция поиска значения в конкретной односвязным списке, используется внутри публичного метода поиска и метода добавления
// ибо перед добавлением надо убедится что элемента с таким значением нет в хеш-таблицы
// если не следить за этим, то при удалении надо будет удалять все элементы с этим значением
// я же решил просто не добавлять дубликаты
// Для удаление реализовал свою функцию поиска, почему в ней напишу

	ElementCollision *current = m_Table[hashIndex(val)].get();		// берем указатель на первый элемент списка (определяем конечно по хеш), не забываем ято он пустой, поэтому проверять его не надо
	std::unique_lock<std::mutex> currentLock(current->mutex);		// захватываем его мьютекс

	while (ElementCollision *next = current->next.get()) {	// берем указатель на следующий элемент списка, при этом если список закончился, то выходим из цикла, ибо указатель nullptr
		std::unique_lock<std::mutex> nextLock(next->mutex);		// захватываем мьютекс следующего элемента
		currentLock.unlock();									// освобождаем мьютекс предыдущего элемента
		if (*(next->value) == val)	// проверяем на равенство значение "следующего" элемента
			return true;
		current = next;					// текущим элементом становится у нас "следующий" элемент
		currentLock = std::move(nextLock);	// ну мьютекс "следующего" элемента мы переносим в текущий
	}

	if (add) {	// если флаг установлен, то добавляем к последнему элементу ключ
	// Добавляется только если по всему списке пришли и не нашли ключа
	// Поэтому ключ повторно не добавляется
		std::unique_ptr<ElementCollision> newCollision(new ElementCollision(val));
		current->next = std::move(newCollision);
	}

// если вышли из цикла, ничего не найдя, то элемента нет в хеш-таблицы
	return false;
}

template<typename Value, std::size_t sizeTable> void ConcurrentHash<Value, sizeTable>::add(const Value &val) {
	foreachList(val, true);	// Для добавления ключа просто вызываем foreachList с флагом добавления true
}

template<typename Value, std::size_t sizeTable> void ConcurrentHash<Value, sizeTable>::remove(const Value &val) {
// Метод удаления реализуем отдельно, ибо полностью проходить по списку не обязательно. Поэтому надо сразу после того найден элемент, удалить его
// Из-за разных подходов к этому и приходится реализововать свой код

// тут как в foreachList
	ElementCollision *currentNode = m_Table[hashIndex(val)].get();
	std::unique_lock<std::mutex> currentLock(currentNode->mutex);

	while (ElementCollision *nextNode = currentNode->next.get()) {
		std::unique_lock<std::mutex> nextLock(nextNode->mutex);
		if (*(nextNode->value) == val) {
// Для удаления просто элементу который стоит указывает на удаляемый элемент, указываем следующим элемент за удаляемым
// так указатель защищен в unique_lock, то утечки памяти не будет
			currentNode->next = std::move(nextNode->next);
			return;
		} else {
			currentNode = nextNode;
			currentLock.unlock();
			currentLock = std::move(nextLock);
		}
	}
}

template<typename Value, std::size_t sizeTable> bool ConcurrentHash<Value, sizeTable>::find(const Value& val) {
	return foreachList(val, false);	// Для поиска указываем что ключ добавлять не надо, если его нет в списке
}

