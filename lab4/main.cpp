#include <atomic>
#include <chrono>
#include <memory>
#include <random>
#include <thread>
#include <vector>
#include <fstream>
#include <iostream>
#include <algorithm>

#include "ConcurentStack.hpp"

concurentStack< std::vector<int> > Stack;
std::atomic<bool> mainFlag;

void consumer(int thr);

int main()
{
	int n;
	std::vector< std::thread > threads;
	std::mt19937 rand(std::chrono::system_clock::now().time_since_epoch().count());

	mainFlag.store(true);

	std::cout << "Enter number consumers: ";
	std::cin >> n;

	for (int i = 0; i < n; ++i)
		threads.push_back(std::thread(consumer, i));

	for (int i = 0; i < 100; ++i) {
		std::vector<int> vector;
		for (int j = 0; j < 1000; ++j)
			vector.push_back(rand());
		Stack.push(vector);
	}

	mainFlag.store(false);

	for (int i = 0; i < n; ++i)
		threads[i].join();

	return 0;
}

void consumer(int thr) {
	std::ofstream outFile("consumer" + std::to_string(thr) + ".txt");
	outFile << "consumer" << std::endl;
	std::shared_ptr< std::vector<int> > vector;
	while (mainFlag.load()) {
		while (vector = Stack.pop()) {
			outFile << "vector" << std::endl;
			std::sort(vector->begin(), vector->end());
			for (auto a: *vector)
				outFile << a << " ";
			outFile << std::endl;
		}
	}
}

