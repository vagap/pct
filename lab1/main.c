#include "hashLibrary.h"
#include "hpctimer.h"

#define N 2000000
#define I 4000000
#define S 6000000
#define D 3000000

#define P 4

struct cellHashTable hashtab[HASHTAB_SIZE];

void* work(void *arg)
{
	unsigned int seed;

	for (int i = 0; i < I / P; ++i)
		hashtab_add(hashtab, rand_r(&seed));

	for (int i = 0; i < S / P; ++i)
		hashtab_lookup(hashtab, rand_r(&seed));

	for (int i = 0; i < D / P; ++i)
		hashtab_delete(hashtab, rand_r(&seed));

	return NULL;
}

int main()
{
	unsigned int i;
	hashtab_init(hashtab);
	pthread_t tids[P];

	for (i = 0; i < N; i++)
		hashtab_add(hashtab, rand());

	double t;

	t = hpctimer_wtime();
	for (i = 0; i < P; i++) {
		cpu_set_t cpu;
		CPU_ZERO(&cpu);
		CPU_SET(i + 1, &cpu);
		pthread_create(&tids[i], NULL, work, NULL);
		pthread_setaffinity_np(tids[i], sizeof(cpu_set_t), &cpu);
	}
	for (i = 0; i < P; i++)
		pthread_join(tids[i], NULL);
	t = hpctimer_wtime() - t;

	printf("%d threads. Elapsed time: %.6f sec.\n", P, t);

	return 0;
}
