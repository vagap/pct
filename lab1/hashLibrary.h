#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#define HASHTAB_SIZE 20000003

struct listnode {
	unsigned int key;
	struct listnode *next;
};

struct cellHashTable{
	struct listnode *beginList;
	pthread_mutex_t mutex;
};

void hashtab_init(struct cellHashTable hashtab[])
{
	for (unsigned int i = 0; i < HASHTAB_SIZE; i++) {
		hashtab[i].beginList = NULL;
		pthread_mutex_init(&hashtab[i].mutex, NULL);
	}
}

//распределение индексов по таблице
unsigned int hashtab_hash(unsigned int key)
{
	unsigned int hash = 0;
	unsigned char *buffer = (unsigned char *)&key;

	for(unsigned int i = 0; i < sizeof(unsigned int); ++i) {
		unsigned char temp = *(buffer++);
		hash += temp;
		hash -= (hash << 13) | (hash >> 19);
	}

	return hash % HASHTAB_SIZE;
}

void hashtab_add(struct cellHashTable hashtab[], unsigned int key)
{
	struct listnode *node;
	int index = hashtab_hash(key);

	pthread_mutex_lock(&hashtab[index].mutex);

	for (node = hashtab[index].beginList; node != NULL; node = node->next) {
		if (node->key == key) {
			pthread_mutex_unlock(&hashtab[index].mutex);
			return;
		}
	}

	node = (struct listnode *)malloc(sizeof(*node));
	if (node != NULL) {
		node->key = key;
		node->next = hashtab[index].beginList;
		hashtab[index].beginList = node;
	}

	pthread_mutex_unlock(&hashtab[index].mutex);
}

int hashtab_lookup(struct cellHashTable hashtab[], unsigned int key)
{
	int index = hashtab_hash(key);
	struct listnode *node;

	pthread_mutex_lock(&hashtab[index].mutex);

	for (node = hashtab[index].beginList; node != NULL; node = node->next) {
		if (node->key == key) {
			pthread_mutex_unlock(&hashtab[index].mutex);
			return 1;
		}
	}

	pthread_mutex_unlock(&hashtab[index].mutex);
	return 0;
}

void hashtab_delete(struct cellHashTable hashtab[], unsigned int key)
{
	int index = hashtab_hash(key);
	struct listnode *p, *prev = NULL;

	pthread_mutex_lock(&hashtab[index].mutex);

	for (p = hashtab[index].beginList; p != NULL; p = p->next) {
		if (p->key == key) {
			if (prev == NULL) {
				hashtab[index].beginList = p->next;
			} else {
				prev->next = p->next;
			}
			free(p);
			pthread_mutex_unlock(&hashtab[index].mutex);
			return;
		}
		prev = p;
	}

	pthread_mutex_unlock(&hashtab[index].mutex);
}
