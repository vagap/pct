#pragma once

#include <array>
#include <atomic>
#include <future>
#include <memory>
#include <mutex>

#include "Parametrs.hpp"

/*
* Расскажу суть, у нас очередь из 5 элементов
* сначало m_popIndex = -1, m_pushIndex = 0
* добавляем элемент в индекс m_pushIndex = 0, после чего m_pushIndex = 1 и m_popIndex становится 0
* добавляем два элемента, сначало записываем в 1 индекс, потом во 2 индекс и pushIndex = 3
* извлекаем все элементы m_popIndex = 0, потом m_popIndex = 1, потом m_popIndex = 2 и после извлечения смотрим m_popIndexдолжен принять значения
* индекса, куда еще не положили значение, то m_popIndex становится -1. Что означает что очередь пуста. Далее добавим элементы:
* pushIndex = 4, pushIndex = 0, pushIndex = 1, pushIndex = 2, pushIndex = 3 и все теперь добавлять нельзя
*/

class Query {	// Класс запроса от жителя к чиновнику
private:
	typedef enum { NONE, FULFILLED, NON_FULFILLED } stateQuery;
// Перечисляемый тип состояния запроса, неопределенный, выполненный и не выполненный

	stateQuery m_state;
	std::promise<void> m_promise;
	std::mutex m_mutex;		// мьютекс для избежание гонки при изменении статуса запроса

public:
	Query(): m_state(NONE), m_promise(), m_mutex() {  };

	std::future<void> get_future() { return m_promise.get_future(); };
	bool captureFromCitizen();		// захват запрос со стороны жителя
	bool captureFromOfficial();		// захват запрос со стороны чиновника
	void beginWork();				// начала работы над запросом. Вызывается чиновником, чтоб сообшить жителю об этом
};

class ConcurrentQueueQuery {
private:
	static const int nonPop = -1;	// специальный член, хранит спец индекс, указывается что очередь пуста
	std::array<std::shared_ptr<Query>, Q> m_queryArray;	// Вектор жесткого заданого размера

	int m_popIndex, m_pushIndex;			// Индекс элементов вектора, куда будет добавлен новый элемент и элемент откуда будет извлечен
	std::mutex m_popMutex, m_pushMutex;		// Мьютексы для извлечения и добавления элементов
	std::condition_variable m_popcond, m_pushcond;	// Условные переменные, указывающие о добавлении нового элемента и извлечения
	bool m_flagEndWork;		// флаг завершения работы

	int getPopIndex();		// Получение индекса для извлечения элемента
	int getPushIndex();		// Получение индекса для добавления элемента
public:
	ConcurrentQueueQuery(): m_popIndex(nonPop), m_pushIndex(0), m_flagEndWork(false) {};

	std::shared_ptr<Query> waitPop();				// извлечение из очереди, с ожиданием, если очередь пуста
	void waitPush(std::shared_ptr<Query> query);	// добавление в очередь, с ожиданием, если очередь заполнена

	void setEnd();		// указывает что работа программы закончилась
	bool isEnded();		// проверка флага результата окончания работы
};
