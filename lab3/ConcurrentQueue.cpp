#include "ConcurrentQueue.hpp"

bool Query::captureFromCitizen() {
	std::lock_guard<std::mutex> lock(m_mutex);
	if (m_state == NONE) {
		m_state = NON_FULFILLED;
		return false;
	} else {
		return true;
	}
}

bool Query::captureFromOfficial() {
	std::lock_guard<std::mutex> lock(m_mutex);
	if (m_state == NONE) {
		m_state = FULFILLED;
		return true;
	} else {
		return false;
	}
}

void Query::beginWork() {
	m_promise.set_value();
}

int ConcurrentQueueQuery::getPopIndex() {
	std::unique_lock<std::mutex> lock(m_popMutex);
	return m_popIndex;
}

int ConcurrentQueueQuery::getPushIndex() {
	std::unique_lock<std::mutex> lock(m_pushMutex);
	return m_pushIndex;
}

std::shared_ptr<Query> ConcurrentQueueQuery::waitPop() {
	std::unique_lock<std::mutex> lock(m_popMutex);
	while ((m_popIndex == nonPop) && !m_flagEndWork)
		m_popcond.wait(lock);

	if (m_flagEndWork)
		return std::shared_ptr<Query> ();

	std::shared_ptr<Query> res = m_queryArray[m_popIndex];
	m_popIndex = (m_popIndex + 1) % Q;

	if (m_popIndex == getPushIndex())
		m_popIndex = nonPop;

	m_pushcond.notify_one();
	return res;
}

void ConcurrentQueueQuery::waitPush(std::shared_ptr<Query> query) {
	std::unique_lock<std::mutex> lock(m_pushMutex);
	while (((m_pushIndex + 1) % Q) == getPopIndex())
		m_pushcond.wait(lock);

	if (m_flagEndWork)
		return;

	m_queryArray[m_pushIndex] = query;
	int old_pushIndex = m_pushIndex;
	m_pushIndex = (m_pushIndex + 1) % Q;
	std::unique_lock<std::mutex> lock2(m_popMutex);
	if (m_popIndex == nonPop)
		m_popIndex = old_pushIndex;
	m_popcond.notify_one();
}

void ConcurrentQueueQuery::setEnd() {
	m_flagEndWork = true;
	m_pushcond.notify_one();
	m_popcond.notify_one();
}

bool ConcurrentQueueQuery::isEnded() {
	return m_flagEndWork;
}

