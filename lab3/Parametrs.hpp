#pragma once

const int Q = 10;
const unsigned int N = 1;
const unsigned int M = 1;
const double Lambda = 3.5;
const unsigned int T = 4000;
const int K = 2;
