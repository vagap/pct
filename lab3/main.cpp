#include <array>
#include <memory>
#include <thread>
#include <random>
#include <iostream>
#include <condition_variable>

#include "Parametrs.hpp"
#include "ConcurrentQueue.hpp"

ConcurrentQueueQuery mainQueue;	// Потокобезопасная очередь, в который хранятся запросы от жителей

class Citizen {
private:
	int m_counterSuccessfulCapture;		// Количество удачно выполненых запросов
	int m_counterUnsuccessfulCapture;	// Количество неудачно выполненых запросов
	int m_number;						// Номер жителя, нужен был мне для отладки, возможно надо будет от него избавится
	std::shared_ptr<Query> m_query;		// Указатель на текущий запрос, который был оставлен этим жителем
	std::future<void> m_future;			// "Будущий результат" (гугли), необходим для информирования чиновником жителя, о начале работы над запросом
	std::exponential_distribution<double> m_distribution;	// член генерации экспоненциального распределения, для определения времени перед генерации запроса
	std::default_random_engine m_randomEngine;				// генератор псевдослучайных чисел, на которых будет работать m_distribution

	void pauseBeforeQuery();		// метод выдерживающий паузу (с экспоненциальным распределением) перед генерацией нового запроса
	void createQuery();				// создание запроса
	void waitBeginWorkOfficial();	// ожидание начала работы чиновника над запросом
	void waitResultWork();			// ожидание времени t для получения результатов работы над запросом
	void checkResultWork();			// проверка результата работы над запросом

public:
	// Конструктор по умолчанию, по хорошему надо в список иниицализации добавить что в комментах.
	Citizen(): m_counterSuccessfulCapture(0), m_counterUnsuccessfulCapture(0), m_distribution(Lambda)/*, m_randomEngine(std::chrono::system_clock::now().time_since_epoch().count())  */ {
		static int k = 0;	// две эти строчки, необходимы для отладки, возможно придется от них отказаться
		m_number = k++;
	};
	void run();				// метод в котором реализован необходимый цикл для иммитации деятельности жителя
};

class Official {
private:
	int m_counterSuccessfulCapture;		// Количество удачно выполненых запросов
	int m_counterUnsuccessfulCapture;	// Количество неудачно выполненых запросов
	int m_number;						// Номер жителя, нужен был мне для отладки, возможно надо будет от него избавится
	std::shared_ptr<Query> m_query;		// Указатель на текущий запрос, который будет обрабатывать этот чиновникы
	std::default_random_engine m_randomEngine;				// генератор псевдослучайных чисел, на которых будет работать m_distribution
	std::uniform_int_distribution<int> m_distribution;		// ты уже знакомо с этим, генерирует случайные числа в указаном диапозоне

	void waitQuery();			// ожидание прихода запроса от жителя
	void work();				// типа работа, состоит просто в ожидание времени [0..2t] 
	void checkResultWork();		// проверка результатов обработки запроса

public:
	Official(): m_counterSuccessfulCapture(0), m_counterUnsuccessfulCapture(0), m_distribution(1, 2 * T)/*, m_randomEngine(std::chrono::system_clock::now().time_since_epoch().count())  */ {
		static int k = 0;	// две эти строчки, необходимы для отладки, возможно придется от них отказаться
		m_number = k++;
	};
	void run();				// метод в котором реализован необходимый цикл для иммитации деятельности жителя
};

// numberCitizen и numberOfficial пременные счетчики оставшихся жителей и чиновников
std::mutex mutexCounter;	// мьютекс для защиты от гонки при изменении количества жителей и чиновников
std::condition_variable condCounter;	// условная переменная для указания что жителей или чиновников стало меньше
int numberCitizen = N, numberOfficial = M;

int main()
{
	std::array<Citizen, N> citizens;	// Массив жестко заданого размера для хранения объектов жителей
	std::array<Official, M> officials;	// Массив чиновников
	std::array<std::thread, N + M> threads;	// Потоки в которых будет крутится жители и чиновники
	int thr = 0;

	for (unsigned int i = 0; i < N; ++i)	// запускаем в отдельных потоках жителей
		threads[thr++] = std::thread(std::bind(&Citizen::run, &citizens[i]));
	for (unsigned int i = 0; i < M; ++i)	// запускаем отдельных потоках чиновников
		threads[thr++] = std::thread(std::bind(&Official::run, &officials[i]));

	std::cout << "Начало" << std::endl;
	{
		std::unique_lock<std::mutex> lock(mutexCounter);	// Стандартная конструкция для работы с условной переменной
		while ((numberCitizen != 0) && (numberOfficial != 0)) {	// Крутим пока чиновники или житили полность разъехались
			condCounter.wait(lock);
		}
	}

	if (numberCitizen == 0) {
		std::cout << "Все жители уехали" << std::endl;
	} else {
		std::cout << "Все чиновники уехали" << std::endl;
	}

	mainQueue.setEnd();		// Даем указание очереди, что больше работать не надо. И все оставшиеся чиновники или жители, перестают работать

	for (unsigned int i = 0; i < N + M; ++i)
		threads[i].join();	// ожидаем завершение всех потоков

    return 0;
}

void Citizen::pauseBeforeQuery() {		// метод выдерживающий паузу (с экспоненциальным распределением) перед генерацией нового запроса
	std::this_thread::sleep_for( std::chrono::milliseconds((int)(1000 * m_distribution(m_randomEngine))) );
}

void Citizen::createQuery() {	// создание запроса
	std::cout << "Создание запроса " << std::endl;

	m_query.reset(new Query);	// создание запроса
	m_future = m_query->get_future();	// назначем будущий результат, необходим для того, чтоб чиновник сообщил жителю, о том, что он начал работать

	mainQueue.waitPush(m_query);	// Помешаем в очередь наш новый запрос
}

void Citizen::waitBeginWorkOfficial() {	// ожидание начала работы чиновника над запросом
	m_future.wait();	// Вот ожидание будущего результата
	m_future.get();		// не шибко нужно, так, для проформы
}

void Citizen::waitResultWork() {	// ожидание времени t для получения результатов работы над запросом
	std::this_thread::sleep_for(std::chrono::milliseconds(T));
	// как я понял из задания "Житель, который оставил заявку, ждёт в течение времени t", что житель обязательно выжидает это время
}

void Citizen::checkResultWork() {	// проверка результата работы над запросом
	if (m_query->captureFromCitizen()) {	// проверяем результат исполнения запроса
		++m_counterSuccessfulCapture;
	} else {
		std::cout << "Заявка гражданин не выплнена" << m_number << std::endl;
		++m_counterUnsuccessfulCapture;
	}
}

void Citizen::run() {
	while (m_counterSuccessfulCapture - m_counterUnsuccessfulCapture < K) {	// Проверяем условия пребывания жителя в городе
		pauseBeforeQuery();
		createQuery();
		waitBeginWorkOfficial();
		waitResultWork();
		checkResultWork();
	}
	mutexCounter.lock();	// Уменьшаем счетчик жителей
	numberCitizen--;
	mutexCounter.unlock();
	condCounter.notify_one();	// и сообщаем в главный поток, что состоялось изменение количества жителей
}

void Official::waitQuery() {
	m_query = mainQueue.waitPop();	// ожидаем когда в очереди появится за
}

void Official::work() {
	std::cout << "Чиновник приступил к работе над заявкой" << std::endl;
	m_query->beginWork();	// вызываем метод, который сообщает о начале работы над запросом
	std::this_thread::sleep_for( std::chrono::milliseconds(m_distribution(m_randomEngine)) );	// засыпаем на случайное время в миллисекундах
}

void Official::checkResultWork() {	// проверяем запрос
	if (m_query->captureFromOfficial()) {
		std::cout << "Чиновник выполнил заявку" << std::endl;
		++m_counterSuccessfulCapture;
	} else {
		++m_counterUnsuccessfulCapture;
	}
}

void Official::run() {
	while (m_counterUnsuccessfulCapture - m_counterSuccessfulCapture < K) {
		waitQuery();
		if (mainQueue.isEnded())
			break;
		work();
		checkResultWork();
	}
	mutexCounter.lock();	// так же как у жителя
	numberOfficial--;
	mutexCounter.unlock();
	condCounter.notify_one();
}

