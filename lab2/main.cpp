#include <signal.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>

#include <string>
#include <vector>
#include <fstream>
#include <random>
#include <chrono>
#include <thread>
#include <iostream>

#define COMPILER "gcc"		// вот тут ты сможешь меня на любой компилятор, на clang ну или любой другой
#define COMPILER_SUFFIX "_compiler"
#define EXEC_SUFFIX "_exec"
#define BASH_SUFFIX "_bash"

void handlingString(const std::string &str, unsigned int task);		// Функция которая производит как раз манипуляции по заданию
void handlingDirectory(const std::string &str, unsigned int task);	// Функция рекурсивного обхода по директориям

void SIGQUIT_Handler(int signo);	// обработчик сигнала SIGQUIT

std::vector<std::thread *> runningThread;	// массив указателей на потоки которые исполняются на каждом ядре
std::vector<std::string> tasksVector;	// Массив строк для тестов

int main()
{
	std::cout << "Programm runnig. PID:" << getpid() << std::endl;

	std::ifstream fileListStr("listStr.txt");

	while (true) {
// Читаем из файла строки в tasksVector
		std::string str;
		getline(fileListStr, str);
		if (!fileListStr.eof())
			tasksVector.push_back(str);
		else
			break;
	}

	signal(SIGQUIT, SIGQUIT_Handler);	// Указываем обработчик для сигнала SIGQUIT

	unsigned int task = 0;	// Порядковый номер задачи исполнения
	std::default_random_engine generator(std::chrono::system_clock::now().time_since_epoch().count());
// генератор псевдослучайных чисел
	std::uniform_int_distribution<int> distribution(0, tasksVector.size() - 1), timeDistribution(1, 1000);
// это приблуда из C++ 11 позволяет генирировать псевдослучайные числа в указаном диапозоне

	runningThread.resize(std::thread::hardware_concurrency());
	for (unsigned i = 0; i < std::thread::hardware_concurrency(); ++i)
		runningThread[i] = nullptr;

	while (1) {
// Цикл бесконечный, программу может прервать только сигнал
		std::string str = tasksVector[distribution(generator)];	// Выбираем случайную строку из набора

		// обходим все исполнительные ядра
		for (unsigned i = 0; i < std::thread::hardware_concurrency(); ++i) {
			if (runningThread[i] != nullptr) {	// ищем первый на котором ничего не запускалось 
				if (runningThread[i]->joinable()) {	// если на ядре что-то запускалось то посмотрим завершилось ли оно
					runningThread[i]->join();	// если завершилось, считываем статус
				} else {
					continue;
				}
			}
			cpu_set_t cpu;	// нашли свободное ядро
			CPU_ZERO(&cpu);
			CPU_SET(i + 1, &cpu); // это дело взято из первой лабы
			runningThread[i] = new std::thread(handlingString, std::ref(str), task);
			// запускаем задачу
			pthread_setaffinity_np(runningThread[i]->native_handle(), sizeof(cpu_set_t), &cpu);
			// и привязываем поток к ядру
			break;
		}
		// Запускаем функцию в отдельном потоке, и указатель на поток помещаем в контейнер
		std::cout << "Task " << task << ". String: " << str << std::endl;
		++task;

		// засыпаем на случайное время в миллисекундах в пределах до 1 секунды
		std::this_thread::sleep_for(std::chrono::milliseconds(timeDistribution(generator)));
	}

	return 0;
}

void handlingString(const std::string &str, unsigned int task) {
	std::string fileName = "task_"+ std::to_string(task);	// префикс 

	if (access(str.c_str(), F_OK) == 0) {
		std::string outputName = "output_" + std::to_string(task);	// имя выходного исполняемого файла
// во всех вызовах system перенаправляется вывод в файл " > " + fileName, и вывод ошибок так же перенаправляется туда " 2>&1"
		if (system(std::string(COMPILER + std::string(" ") + str + " -o " + outputName + " > " + fileName + COMPILER_SUFFIX + " 2>&1").c_str()) == 0) {
		// если прога завершилась со статусом 0, то удалось файл скомпилить, и мы его заменим
			system(std::string("./" + outputName + " > " + fileName + EXEC_SUFFIX + " 2>&1").c_str());
		} else {
			struct stat fileStat;
			stat(str.c_str(), &fileStat);	// берем информацию о файле

			if (S_ISDIR(fileStat.st_mode)) {	// смотрим, а не директория ли
				handlingDirectory(str, task);	// если да, вызываем рекурсивный обход
			}
		}
	} else {
		system(std::string(str + " > " + fileName + BASH_SUFFIX + " 2>&1").c_str());	// если не файл, просто вызываем как есть, с перенаправлением вывода
	}
}

void handlingDirectory(const std::string &str, unsigned int task) {
	chdir(str.c_str());	// меняем текущию директорию на имя переданной директории
	DIR *directory = opendir(".");	// считываем содержимое новой текущий директории
	struct dirent *entry;

	while ((entry = readdir(directory)) != NULL) {
		if ((entry->d_name != std::string(".")) && (entry->d_name != std::string("..")))	// для директорий . и .. функция не вызывается
			handlingString(entry->d_name, task);
	}

	closedir(directory);
	chdir("..");	// переходим в родительскую директорию
}

void SIGQUIT_Handler(int signo) {
	for (unsigned i = 0; i < std::thread::hardware_concurrency(); ++i)
		if (runningThread[i] != nullptr)
			runningThread[i]->join();	// дожидаемся завершения потоков на всех ядрах

	exit(0);
}

